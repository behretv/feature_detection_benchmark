# feature_detection_benchmark
## Summary
Feature detection benchmark comparing AKAZE, BRSIK, ORB and SIFT. Includes a 
parametrization workflow for all four approaches. The parametrization is 
optimizing the inlier per second (IPS), as I described in my 
[master's thesis](https://gitlab.com/behretv/feature_detection_benchmark/blob/master/master_thesis.pdf)
the IPS is a good measure to parametrize the feature detection for indirect SLAM,
such as ORB SLAM.

## Installation
### Dependencies
Before the project can be build OpenCV including the contrib packages need to 
be installed. On the following webpage an installation guide is provided, which 
worked for Ubuntu 16.04 LTS and 18.04 LTS:

[Installing OpenCV](https://www.learnopencv.com/install-opencv3-on-ubuntu/)

However, instead of using the build descriped in the link above, I used the 
following options:
```
$ cmake -D CMAKE_BUILD_TYPE=RELEASE \
        -D CMAKE_INSTALL_PREFIX=$cwd/installation/OpenCV-"$cvVersion" \
        -D INSTALL_C_EXAMPLES=ON \
        -D INSTALL_PYTHON_EXAMPLES=ON \
        -D WITH_TBB=ON \
        -D WITH_V4L=ON \
        -D OPENCV_PYTHON3_INSTALL_PATH=$cwd/OpenCV-$cvVersion-py3/lib/python3.5/site-packages \
        -D WITH_QT=ON \
        -D WITH_OPENGL=ON \
        -D OPENCV_ENABLE_NONFREE=True \
        -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules \
        -D BUILD_EXAMPLES=ON ..
```
If you have an internal compile error disable pre comiled headers by adding:
```
-D ENABLE_PRECOMPILED_HEADERS=OFF
```

### Building
First create a build folder and navigate into it
```
$ mkdir build 
$ cd build
```
then execute cmake
```
$ cmake ..
```
and finally build the project
```
$ make
```

## Run Benchmark
In order to run a benchmark the paths for the images and where the results should be saved have to be adjusted:
*  path.img = path where the image folder **image_0/** and the time stamp file **times.txt** lie
*  path.dat = path where the results should be saved

The images have to be named from 000000.png to 999999.png and need to lie in the 
folder **image_0/**. Additionally time stamps need to be provided for each 
image in a **times.txt** file. If you do not want to use the images for SLAM, 
you can just give each image an abritrary time stamp.
