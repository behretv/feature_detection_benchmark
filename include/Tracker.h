//
// Created by behretv on 13.09.18.
//

#ifndef VIGA2EM_SLAM_CPP_TRACKER_H
#define VIGA2EM_SLAM_CPP_TRACKER_H


#include <opencv2/opencv.hpp>
#include <vector>
#include <iostream>
#include <iomanip>
#include "Stats.h"

using namespace std;
using namespace cv;

class Tracker{
//=================================================================================================
// Public
public:
    // Constructor
    Tracker(Ptr<Feature2D> detector, Ptr<DescriptorMatcher> matcher);

    // Destructor
    ~Tracker();

    // Methods
    void Process(Mat frame);
    void DrawBoundingBox(Mat image, vector<Point2f> bb);
    vector<Point2f> Points(vector<KeyPoint> keypoints);

    // Get
    Stats GetStats(){return _stats;}
    Mat GetResult(){return _res;}

    // Set
    void SetFirstFrame(Mat frame, vector<Point2f> bb, Stats& stats);
    void SetFPS(double sec) {_stats.fps = sec;}
    void SetVisualization(bool vis) {_vis = vis;}

//=================================================================================================
// Protected
protected:
    // Attributes
    Mat _firstFrame, _firstDesc;
    Mat _homography;
    Mat _res;
    Ptr<Feature2D> _detector;
    Ptr<DescriptorMatcher> _matcher;
    vector<KeyPoint> _firstKP;
    vector<Point2f> _objectBB;
    vector<KeyPoint> _inliers1;
    vector<KeyPoint> _inliers2;
    vector<DMatch> _inlierMatches;
    Stats _stats;

    // Constant attributes
    const double _ransacThresh = 2.5f; // RANSAC inlier threshold
    const double _nnMatchRatio = 0.8f; // Nearest-neighbour matching ratio
    const int _bbMinInliers = 20; // Minimal number of inliers to draw bounding box

    // Colors
    const Scalar _colorBBox = {0,0,200};
    const Scalar _colorMatches = {0,200,0};
    const Scalar _colorPoints = {255,0,0};

    // Set
    void SetMatcher(Ptr<DescriptorMatcher> & value){this->_matcher = move(value);}
    void SetDetector(Ptr<Feature2D> & value){this->_detector = move(value);}

private:
    // Attributes
    bool _vis;

    // Methods
    void ComputeMatches(Mat& frame,vector<KeyPoint>& x1,vector<KeyPoint>& x2);
    void DetectInliers(Mat& frame, vector<KeyPoint>& x1,vector<KeyPoint>& x2);
    void GenerateVisualization(Mat& frame);
};


#endif //VIGA2EM_SLAM_CPP_TRACKER_H
