//
// Created by behretv on 03.01.19.
//

#ifndef FEATURE_BENCHMARK_AKAZE_H
#define FEATURE_BENCHMARK_AKAZE_H

#include "Feature.h"

class Akaze : public Feature {
public:
    // Constructor & destructor
    explicit Akaze(Parameter* params);
    ~Akaze();

    // Set
    void setTracker();
    void setParameters(Parameter* params);

    // Get
    string getParameterString() override;

private:
    // Attributes
    Ptr<AKAZE> _akaze;
};


#endif //FEATURE_BENCHMARK_AKAZE_H
