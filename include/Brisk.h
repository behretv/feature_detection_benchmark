//
// Created by behretv on 03.01.19.
//

#ifndef FEATURE_BENCHMARK_BRISK_H
#define FEATURE_BENCHMARK_BRISK_H

#include "Feature.h"

class Brisk : public Feature{
public:
    // Constructor & destructor
    explicit Brisk(Parameter* params);
    ~Brisk();

    // Set
    void setTracker();
    void setParameters(Parameter* params) override;

    // Get
    string getParameterString() override;
private:
    // Attributes
    Ptr<BRISK> _brisk;
};


#endif //FEATURE_BENCHMARK_BRISK_H
