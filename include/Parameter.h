//
// Created by behretv on 02.01.19.
//
#ifndef FEATURE_BENCHMARK_PARAMETER_H
#define FEATURE_BENCHMARK_PARAMETER_H

#include <vector>
using namespace std;

class Parameter{
public:
    Parameter() = default;
    ~Parameter() = default;

    // Default parameters are written in the comments within [x]
    //=============================================================================================
    struct {
        // Default scalar value
        int   _1_descriptorSize = 0;         // [0] Size of the descriptor in bits. 0 -> Full size
        int   _2_descriptorChannels = 3;     // [3] Number of channels in the descriptor (1, 2, 3)
        float _3_threshold = 0.0005f;       // [0.001] Detector response threshold to accept point
        int   _4_nOctaves = 3;                // [4] Maximum octave evolution of the image
        int   _5_nOctaveLayers = 4;           // [4] Default number of sublevels per scale level

        // Iteration arrays
        vector<int>   _1_array = {0};
        vector<int>   _2_array = {3};
        vector<float> _3_array = {0.8f,0.9f,1.0f,1.1f};
        vector<int>   _4_array = {-1,0,1};
        vector<int>   _5_array = {0,1,2,3,4,5};
    } _akaze;
    /// AKAZE( 0| 3|0.0001|3|5) = {85,0.3652,1.063} -> max
    //=============================================================================================
    struct {
        // Default scalar values
        int _1_threshold = 40;             // [30] AGAST detection threshold score.
        int _2_octaves = 6;             // [3] detection octaves. Use 0 to do single scale.
        float _3_patternScale = 1.68f;   // [1.0] apply this scale to the pattern used for sampling the neighbourhood of a keypoint.

        // Iteration arrays
        vector<int>   _1_array = {-10,0,10};
        vector<int>   _2_array = {0,1,2};
        vector<float> _3_array = {0.8f,1.0f,1.2f,1.4f};
    } _brisk;
    /// BRSIK(30| 3|1.2) = {82,0.4485,1.605} -> max
    //=============================================================================================
    struct {
        // Default scalar values
        int _1_nFeatures = 2000;        // [500] The maximum number of features to retain.
        float _2_scaleFactor = 1.2f;    // [1.2] Pyramid decimation ratio, greater than 1. scaleFactor==2 means the classical pyramid, where each next level has 4x less pixels than the previous, but such a big scale factor will degrade feature matching scores dramatically. On the other hand, too close to 1 scale factor will mean that to cover certain scale range you will need more pyramid levels and so the speed will suffer.
        int _3_nLevels = 10;             // [8] The number of pyramid levels. The smallest level will have linear size equal to input_image_linear_size/pow(scaleFactor, nlevels - firstLevel).
        int _4_edgeThreshold = 33;      // [31] This is size of the border where the features are not detected. It should roughly match the patchSize parameter.
        int _5_firstLevel = 0;	        // [0] The level of pyramid to put source image to. Previous layers are filled with upscaled source image.
        int _6_WTA_K = 2;               // [2] The number of points that produce each element of the oriented BRIEF descriptor. The default value 2 means the BRIEF where we take a random point pair and compare their brightnesses, so we get 0/1 response. Other possible values are 3 and 4. For example, 3 means that we take 3 random points (of course, those point coordinates are random, but they are generated from the pre-defined seed, so each element of BRIEF descriptor is computed deterministically from the pixel rectangle), find point of maximum brightness and output index of the winner (0, 1 or 2). Such output will occupy 2 bits, and therefore it will need a special variant of Hamming distance, denoted as NORM_HAMMING2 (2 bits per bin). When WTA_K=4, we take 4 random points to compute each bin (that will also occupy 2 bits with possible values 0, 1, 2 or 3).
        int _7_patchSize = 31;          // [31] size of the patch used by the oriented BRIEF descriptor. Of course, on smaller pyramid layers the perceived image area covered by a feature will be larger.
        int _8_fastThreshold = 20;      // [20]

        // Iteration arrays
        vector<int>   _1_array = {2200,2300,2500,2700};
        vector<float> _2_array = {0.9f,1.0f,1.2f};
        vector<int>   _3_array = {0,1,2};
        vector<int>   _4_array = {-2,-1,0,1,2};
        vector<int>   _5_array = {0};
        vector<int>   _6_array = {2,3};
        vector<int>   _7_array = {31,32};
        vector<int>   _8_array = {-2,-1,0,1,2};

    } _orb;
    /// First iteration optimum:
    /// ORB(2200|1.1|10|33|0|3|31|18) = {67,0.2356,2.965} -> max
    /// ORB(2500|1.1|10|33|0|3|31|22) = {74,0.226,1.89} -> max
    //=============================================================================================
    struct {
        // Default scalar values
        int _1_nFeatures = 0;	            // [0] The number of best features to retain. The features are ranked by their scores (measured in SIFT algorithm as the local contrast)
        int _2_nOctaveLayers = 3;	        // [3] The number of layers in each octave. 3 is the value used in D. Lowe paper. The number of octaves is computed automatically from the image resolution.
        double _3_contrastThreshold = 0.04; // [0.04] The contrast threshold used to filter out weak features in semi-uniform (low-contrast) regions. The larger the threshold, the less features are produced by the detector.
        double _4_edgeThreshold = 10;	    // [10] The threshold used to filter out edge-like features. Note that the its meaning is different from the contrastThreshold, i.e. the larger the edgeThreshold, the less features are filtered out (more features are retained).
        double _5_sigma = 1.6;	            // [1.6] The sigma of the Gaussian applied to the input image at the octave #0. If your image is captured with a weak camera with soft lenses, you might want to reduce the number.

        // Iteration arrays
        vector<int>    _1_array = {0,1500};
        vector<int>    _2_array = {0};
        vector<double> _3_array = {0.5,1.0,2.0};
        vector<double> _4_array = {0.5,1.0,2.0};
        vector<double> _5_array = {0.5,1.0,2.0};
    } _sift;
    /// SIFT(0|2|0.02|10|1.6) = {127,0.316,1.258} -> max
    /// SIFT(0|2|0.02|20|1.6) = {135,0.3183,1.062}
};
#endif //FEATURE_BENCHMARK_PARAMETER_H
