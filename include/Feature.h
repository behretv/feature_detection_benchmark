//
// Created by behretv on 03.01.19.
//

#ifndef FEATURE_BENCHMARK_FEATURE_H
#define FEATURE_BENCHMARK_FEATURE_H

#pragma once
// Include standard global libraries
#include <string>
#include <iostream>
#include <vector>

// Include specific gloab labraries
#include <opencv2/opencv.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <iomanip>

// Include specific local libraries or classes
#include "Tracker.h"
#include "Parameter.h"
#include "Stats.h"

class Feature {
public:
    // Constructor & Destructor
    Feature();
    ~Feature();

    // Methods
    bool Update();

    void ClearData();

    // Get
    Stats getCurrentStats() const {return _tracker->GetStats();}
    int getMedianInlier() {return Median(_inliers);}
    double getMedianFPS(){return Median(_fps);}
    double getMedianTFPS(){return Median(_ips);}
    double getMedianRatio(){return Median(_ratio);}
    string getStatisticsString();
    virtual string getParameterString() = 0;

    // Set
    void setCurrentFrame(Mat input_frame);
    void setlog(bool mode){_log = mode;}
    void setVisualization(bool mode);
    virtual void setParameters(Parameter* params) = 0;
protected:
    // Object Pointer
    Tracker*_tracker;
    Parameter* _parameter;
    string _type;
private:
    // Methods
    void ApplyOnImage();
    void ShowImagePair();

    // Template
    template<typename T> T Median(vector<T> array);

    // Scalar Attributes
    bool _vis;
    bool _log;
    int _idx;
    Mat _currentFrame;
    Mat _previousFrame;

    // Vector Attributes
    vector<int> _inliers;
    vector<double> _fps;
    vector<double> _ips;
    vector<double> _ratio;
    vector<Point2f> _boundingBox;
};


#endif //FEATURE_BENCHMARK_FEATURE_H
