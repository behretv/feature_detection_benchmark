//
// Created by behretv on 03.01.19.
//

#ifndef FEATURE_BENCHMARK_ORB_H
#define FEATURE_BENCHMARK_ORB_H

#include "Feature.h"

class Orb : public Feature {
public:
    // Constructor & destructor
    explicit Orb(Parameter* params);
    ~Orb();

    // Set
    void setTracker();
    void setParameters(Parameter* params) override;

    // Get
    string getParameterString() override;

private:
    // Attributes
    Ptr<ORB> _orb;
};


#endif //FEATURE_BENCHMARK_ORB_H
