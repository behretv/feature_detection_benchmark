//
// Created by behretv on 03.01.19.
//

#ifndef FEATURE_BENCHMARK_SIFT_H
#define FEATURE_BENCHMARK_SIFT_H

#include "Feature.h"

class Sift : public Feature {
public:
    // Constructor & destructor
    explicit Sift(Parameter* params);
    ~Sift();

    // Set
    void setTracker();
    void setParameters(Parameter* params) override;

    // Get
    string getParameterString() override;

private:
    // Attributes
    Ptr<Feature2D> _sift;
};


#endif //FEATURE_BENCHMARK_SIFT_H
