//
// Created by behretv on 13.09.18.
//

#ifndef VIGA2EM_SLAM_CPP_LOGGER_H
#define VIGA2EM_SLAM_CPP_LOGGER_H


// Global standard libraries
#include <string.h>
#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <sstream>

#include <opencv2/opencv.hpp>

// Local specific libraries
#include "Stats.h"

using namespace std;
using namespace cv;

class Logger{
//=================================================================================================
// Public
public:
    // Constructor
    explicit Logger(string& folder_str,const string& append_str);

    // Destructor
    ~Logger();

    // Methods
    void Statistics(string type, const Stats& stats);
    void PointPairs(const vector<KeyPoint> & x1, const vector<KeyPoint> & x2);

//=================================================================================================
// Private
private:
    // Handles for the files
    FILE* _orbFile;     // ORB file
    FILE* _akzFile;     // AKAZE file
    FILE* _brkFile;     // BRISK file
    FILE* _sftFile;     // SIFT file
    FILE* _ptpFile;     // Point pairs file

    // File names including the path
    string _folderPath;
    const string _orbFileName = "orb.log";
    const string _akzFileName = "akaze.log";
    const string _brkFileName = "brisk.log";
    const string _sftFileName = "sift.log";
    const string _ptpFileName = "point_pairs.log";
};


#endif //VIGA2EM_SLAM_CPP_LOGGER_H
