//
// Created by behretv on 03.01.19.
//

#include "Sift.h"

//=================================================================================================
Sift::Sift(Parameter* params){
    // Type flag
    _type = "SIFT";

    // Create object
    _sift = xfeatures2d::SIFT::create(
            params->_sift._1_nFeatures,
            params->_sift._2_nOctaveLayers,
            params->_sift._3_contrastThreshold,
            params->_sift._4_edgeThreshold,
            params->_sift._5_sigma);

    // Set parameters
    _parameter = params;
    this->setTracker();
}
//=================================================================================================
Sift::~Sift(){
    if(_sift){
        delete [] _tracker;
        delete [] _sift;
    }
}
//=================================================================================================
void Sift::setTracker() {
    Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("BruteForce");
    _tracker = new Tracker(_sift,matcher);
}
void Sift::setParameters(Parameter *params) {
    // Create object
    _sift = xfeatures2d::SIFT::create(
            params->_sift._1_nFeatures,
            params->_sift._2_nOctaveLayers,
            params->_sift._3_contrastThreshold,
            params->_sift._4_edgeThreshold,
            params->_sift._5_sigma);

    // Set parameters & tracker
    _parameter = params;
    this->setTracker();
}
//=================================================================================================
string Sift::getParameterString() {
    stringstream info_str;
    info_str << setprecision(4) <<" SIFT(" <<
    _parameter->_sift._1_nFeatures << "|" <<
    _parameter->_sift._2_nOctaveLayers << "|" <<
    _parameter->_sift._3_contrastThreshold << "|" <<
    _parameter->_sift._4_edgeThreshold << "|" <<
    _parameter->_sift._5_sigma << ")";

    return info_str.str();
}