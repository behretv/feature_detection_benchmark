//
// Created by behretv on 13.09.18.
//

#include "Logger.h"

//-------------------------------------------------------------------------------------------------
Logger::Logger(string &str_folder,const string &append_str):_folderPath(str_folder)
{
    // Console output
    cout << "---------------------------------" << endl;
    cout << "+++ Activate Logger +++" << endl;
    cout << "Create log files:" << endl;
    cout << _folderPath+append_str+_orbFileName << endl;
    cout << _folderPath+append_str+_akzFileName << endl;
    cout << _folderPath+append_str+_brkFileName << endl;
    cout << _folderPath+append_str+_sftFileName << endl;
    cout << _folderPath+append_str+_ptpFileName << endl;

    // Create new files
    _orbFile = fopen((_folderPath+append_str+_orbFileName).c_str(), "w");
    _akzFile = fopen((_folderPath+append_str+_akzFileName).c_str(), "w");
    _brkFile = fopen((_folderPath+append_str+_brkFileName).c_str(), "w");
    _sftFile = fopen((_folderPath+append_str+_sftFileName).c_str(), "w");
    _ptpFile = fopen((_folderPath+append_str+_ptpFileName).c_str(), "w");

    // Check if the files could be created
    if(_orbFile== nullptr){cout << "File could not be opened" << endl;}

    // Open files in "append" mode
    _orbFile = fopen((_folderPath+append_str+_orbFileName).c_str(), "a");
    _akzFile = fopen((_folderPath+append_str+_akzFileName).c_str(), "a");
    _brkFile = fopen((_folderPath+append_str+_brkFileName).c_str(), "a");
    _sftFile = fopen((_folderPath+append_str+_sftFileName).c_str(), "a");
    _ptpFile = fopen((_folderPath+append_str+_ptpFileName).c_str(), "a");
    cout << "---------------------------------" << endl;;
}

//-------------------------------------------------------------------------------------------------
Logger::~Logger(){
    fclose(_orbFile);
    fclose(_akzFile);
    fclose(_brkFile);
    fclose(_sftFile);
    fclose(_ptpFile);
}


//-------------------------------------------------------------------------------------------------
// STATISTICS
void Logger::Statistics(string type, const Stats& stats)
{
    string data_str;
    stringstream data;
    data << stats.matches << " | " <<
         stats.inliers << " | " << setprecision(2) <<
         stats.ratio   << " | " << std::fixed << setprecision(2) <<
         stats.fps;
    data_str = data.str();

    if(_orbFile==nullptr){cout << "File could not be opened" << endl;}

    // Open log file in "append" mode
    if(type == "ORB"){
        fprintf(_orbFile, "%s\n",data_str.c_str());
    }
    if(type == "AKAZE"){
        fprintf(_akzFile, "%s\n",data_str.c_str());
    }
    if(type == "BRISK"){
        fprintf(_brkFile, "%s\n",data_str.c_str());
    }
    if(type == "SIFT"){
        fprintf(_sftFile, "%s\n",data_str.c_str());
    }

    //cout << type <<" - Append data string: \t" << data_str << endl;
}

//-------------------------------------------------------------------------------------------------
// Point pairs
void Logger::PointPairs(const vector<KeyPoint> & x1, const vector<KeyPoint> & x2){
    // Convert keypoints
    vector<Point2f> v1,v2;
    KeyPoint::convert(x1, v1, std::vector< int >());
    KeyPoint::convert(x2, v2, std::vector< int >());


    string data_str;
    stringstream data;

    for (int i=0; i<v1.size(); i++){
        data << v1[i].x << " | " << v1[i].y  << " | " << v2[i].x << " | "  << v2[i].y << " \n " ;
        data_str = data.str();
    }
    fprintf(_ptpFile, "%s\nnext",data_str.c_str());

}