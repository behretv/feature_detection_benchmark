//
// Created by behretv on 13.09.18.
//

#include "Tracker.h"


//=================================================================================================
// CONSTRUCTOR
Tracker::Tracker(Ptr<Feature2D> detector, Ptr<DescriptorMatcher> matcher):
    _vis(false)
{
    // Set by move
    SetMatcher(matcher);
    SetDetector(detector);
}

//=================================================================================================
// DESTRUCTOR
Tracker::~Tracker() = default;

//=================================================================================================
// Methods

//-------------------------------------------------------------------------------------------------
// SETFISRTFRAME
void Tracker::SetFirstFrame(Mat frame, vector<Point2f> bb, Stats& stats)
{
    // Set first frame
    _firstFrame = frame.clone();

    // Set first KP & desc
    _detector->detectAndCompute(_firstFrame, noArray(), _firstKP, _firstDesc);

    // Inits stats
    stats.keypoints = (int)_firstKP.size();

    // Set Boundingbox - BB
    DrawBoundingBox(_firstFrame, bb);
    _objectBB = bb;
}

//-------------------------------------------------------------------------------------------------
// PROCESS
void Tracker::Process(Mat frame) {
    // Declare local variables
    Mat inlier_mask, homography;
    vector<KeyPoint> matched1, matched2, kp, in1, in2;
    vector<Point2f> new_bb;
    vector<DMatch> in_matches;

    // Update detector and stats
    ComputeMatches(frame,matched1,matched2);
    DetectInliers(frame,matched1,matched2);
    perspectiveTransform(_objectBB, _objectBB, _homography);

    // Visualization if wanted
    if(_vis) GenerateVisualization(frame);
}
//-------------------------------------------------------------------------------------------------
// DRAWBOUNDINGBOX
void Tracker::DrawBoundingBox(Mat image, vector<Point2f> bb){
    for(unsigned i = 0; i < bb.size() - 1; i++) {
        line(image, bb[i], bb[i + 1], _colorBBox, 2);
    }
    line(image, bb[bb.size() - 1], bb[0], _colorBBox, 2);
}
//-------------------------------------------------------------------------------------------------
vector<Point2f> Tracker::Points(vector<KeyPoint> keypoints){
    vector<Point2f> res;
    for(auto keypt : keypoints){
        res.push_back(keypt.pt);
    }
    return res;
}
//-------------------------------------------------------------------------------------------------
void Tracker::ComputeMatches(Mat& frame,vector<KeyPoint>& x1,vector<KeyPoint>& x2){
    // Declaration
    Mat desc;
    vector<KeyPoint> kp;
    vector<vector<DMatch> > matches;

    _detector->detectAndCompute(frame, noArray(), kp, desc);
    _matcher->knnMatch(_firstDesc, desc, matches, 2);
    _stats.keypoints = (int) kp.size();

    for(auto m : matches) {
        if(m[0].distance < _nnMatchRatio * m[1].distance) {
            x1.push_back(_firstKP[m[0].queryIdx]);
            x2.push_back(      kp[m[0].trainIdx]);
        }
    }

    _stats.matches = (int) x1.size();
}
void Tracker::DetectInliers(Mat& frame, vector<KeyPoint> &matched1, vector<KeyPoint> &matched2) {
    // Declaration
    Mat inlier_mask, homography;
    vector<KeyPoint>  in1, in2;
    vector<Point2f> new_bb;
    vector<DMatch> in_matches;

    // Find inliners
    if (matched1.size() >= 4) {
        _homography = findHomography(Points(matched1), Points(matched2),
                                     RANSAC, _ransacThresh, inlier_mask);
    }

    //.............................................................................................
    // If there are not any matches
    if (matched1.size() < 4 || _homography.empty()) {
        cout << " - No matches - size: " << matched1.size();
        hconcat(_firstFrame, frame, _res);
        _stats.inliers = 0;
        _stats.ratio = 0;
    }
    //.............................................................................................
    else {
        // Create inlier vector
        for (unsigned i = 0; i < matched1.size(); i++) {
            if (inlier_mask.at<uchar>(i)) {
                int new_i = static_cast<int>(in1.size());
                in1.push_back(matched1[i]);
                in2.push_back(matched2[i]);
                in_matches.emplace_back(DMatch(new_i, new_i, 0));
            }
        }
        // Save stats
        _stats.inliers = (int) in1.size();
        _stats.ratio = _stats.inliers * 1.0 / _stats.matches;
        _inliers1 = in1;
        _inliers2 = in2;
        _inlierMatches = in_matches;
    }
}
void Tracker::GenerateVisualization(Mat &frame) {
    Mat frame_with_bb = frame.clone();
    if (_stats.inliers >= _bbMinInliers) {
        DrawBoundingBox(frame_with_bb, _objectBB);
        _res = frame.clone();
        //drawMatches(_firstFrame, _inliers1, frame_with_bb, _inliers2, _inlierMatches, _res,
          //          _colorMatches, _colorPoints, std::vector<char>(), 0);
    }
}

