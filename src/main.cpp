// Global libraries
// A-F
#include <algorithm>
#include <ctype.h>
#include <cstdlib>
#include <deque>
#include <errno.h>
#include <fstream>
// G-L
#include <iostream>
// M-R
// S-Z
#include <signal.h>
#include <sstream>
#include <stdlib.h>
#include <thread>
#include <typeinfo>
#include <type_traits>
#include <unistd.h>

// Local libraries by path
#include <opencv2/opencv.hpp>

// Local header files
#include "Feature.h"
#include "Akaze.h"
#include "Brisk.h"
#include "Orb.h"
#include "Sift.h"
#include "Logger.h"
#include "Parameter.h"

// Namespaces
using namespace std;
using namespace cv;

//==================================================================================================
// Macros
#define SAVE_RESULTS false
#define MAX_NUMBER_PIXEL 4e5f

//==================================================================================================
// Declarations

// -------------------------------------------------------------------------------------------------
// Classes
struct Path{string img; string dat;} path;
template <typename T> struct ThreadData {
    int idx = 0;
    vector<string> files;
    Logger* log = nullptr;
    T *feature = nullptr;
    Path path;
};
struct Threshold{
    int inlier = 0;
    double fps = 0;
    double ips = 0;
};
//-------------------------------------------------------------------------------------------------
// Methods
Mat GrabImage(string name);
void LoadImages(const string &path, vector<string> &file_names, vector<double> &time_stamps);
void Iterate(ThreadData<Akaze>* tda);
void Iterate(ThreadData<Brisk>* tdb);
void Iterate(ThreadData<Orb>* tdo);
void Iterate(ThreadData<Sift>* tds);
template<typename T> static void Loop(T* td);
template<typename T> static void Check4Max(T *td, Threshold &t);
//==================================================================================================
// MAIN
int main(int argc, char *argv[]) {
    // Adjust me:
    // getenv("HOME") is your hzome dir under linux
    path.img = string(getenv("HOME"))+"/XXXXX/images";
    path.dat = string(getenv("HOME"))+"/XXXXX/data";
    
    
    // Ask user what to do:
    string ite, thr, opt, img, sav, dtl;
    cout << "Iterate data? (y/n) "; cin >> ite;

    if (ite != "y"){
        cout << "Showing more debug options? (y/n) "; cin >> opt;}

    if(opt == "y"){
        cout << "Showing image stream? (y/n) "; cin >> img;
        cout << "Saving data? (y/n) "; cin >> sav;
        cout << "Show log details? (y/n) "; cin >> dtl;
    }

    // Path's
    string img_path, dat_path;
  
    
    // Image files
    vector<string> vstrImageFilenames;
    vector<double> vTimestamps;
    LoadImages(path.img, vstrImageFilenames, vTimestamps);
    auto ptrLog = new Logger(path.dat, "0000");
    auto ptrPar = new Parameter();

    //_____________________________________________________________________________________________
    // Data structs
    auto *tda = new ThreadData<Akaze>;
    tda->files = vstrImageFilenames;
    tda->feature = new Akaze(ptrPar);

    auto *tdb = new ThreadData<Brisk>;
    tdb->files = vstrImageFilenames;
    tdb->feature = new Brisk(ptrPar);

    auto *tdo = new ThreadData<Orb>;
    tdo->files = vstrImageFilenames;
    tdo->feature = new Orb(ptrPar);

    auto *tds = new ThreadData<Sift>;
    tds->files = vstrImageFilenames;
    tds->feature = new Sift(ptrPar);

    if(sav == "y"){
        tda->log = ptrLog;
        tdb->log = ptrLog;
        tdo->log = ptrLog;
        tds->log = ptrLog;
    }
    if(img == "y"){
        tda->feature->setVisualization(true);
        tdb->feature->setVisualization(true);
        tdo->feature->setVisualization(true);
        tds->feature->setVisualization(true);
    }
    if(dtl == "y"){
        tda->feature->setlog(true);
        tdb->feature->setlog(true);
        tdo->feature->setlog(true);
        tds->feature->setlog(true);
    }

    //_____________________________________________________________________________________________
    // Default parameters

    std::cout << std::fixed;
    std::cout << std::setprecision(2);

    if (ite == "y") {
        cout << "Results iterating parameters:" << endl;
        thread th4([](ThreadData<Akaze>* t1){ return Iterate(t1); },tda);
        thread th5([](ThreadData<Orb>*   t2){ return Iterate(t2); },tdo);
        thread th6([](ThreadData<Brisk>* t3){ return Iterate(t3); },tdb);
        Iterate(tds);

        // Wait for the threads to finish
        th4.join();
        th5.join();
        th6.join();
    }
    else {
        cout << "Results with default parameters:" << endl;
        thread th1(Loop<ThreadData<Akaze>>,tda);
        thread th2(Loop<ThreadData<Orb>>,tdo);
        thread th3(Loop<ThreadData<Brisk>>,tdb);
        Loop<ThreadData<Sift>>(tds);

        // Print results
        cout << tda->feature->getParameterString() << tda->feature->getStatisticsString() << endl;
        cout << tdb->feature->getParameterString() << tdb->feature->getStatisticsString() << endl;
        cout << tdo->feature->getParameterString() << tdo->feature->getStatisticsString() << endl;
        cout << tds->feature->getParameterString() << tds->feature->getStatisticsString() << endl;

        // Wait for the threads to finish
        th1.join();
        th2.join();
        th3.join();
    }

    // Call destructors
    if (tda->log) tda->log->~Logger();
    if (tdb->log) tdb->log->~Logger();
    if (tdo->log) tdo->log->~Logger();
    if (tds->log) tds->log->~Logger();

  return 0;
}
//==================================================================================================
// Template functions
//-------------------------------------------------------------------------------------------------
// Loop
template<typename T> static void Loop(T* td){
    string type;
    if (is_same<T,ThreadData<Akaze>>::value ) type = "AKAZE";
    if (is_same<T,ThreadData<Brisk>>::value ) type = "Brisk";
    if (is_same<T,ThreadData<Orb>>::value ) type = "Orb";
    if (is_same<T,ThreadData<Sift>>::value ) type = "Sift";

    // Init image data storage
    unsigned long i, n;
    n = td->files.size();
    i = 0;
    while (i < n) {
        // Get frame from video stream
        Mat frame = GrabImage(td->files[i]);
        ++i;

        //.........................................................................................
        // Main process within try
        try {
            // +++ Feature detection +++
            td->feature->setCurrentFrame(frame);
            td->feature->Update();


            printf("\rProgress(%lu/%lu) ",i,n);
            cout << flush;
        }
        catch(...) {
            cerr << td->feature->getParameterString() << " -> fails! " << endl;
                break;
        }
        //.........................................................................................
        if(td->log){
            td->log->Statistics(type,td->feature->getCurrentStats());
        }
    }
}
//-------------------------------------------------------------------------------------------------
template<typename T> static void Check4Max(T *td, Threshold &t) {
    td->idx++;
    int mInl = td->feature->getMedianInlier();
    double mFPS = td->feature->getMedianFPS();
    double mIPS = td->feature->getMedianTFPS();
    if(td->idx == 1){
        t.inlier = mInl;
        t.fps = mFPS;
        t.ips = mIPS;
        printf("[%03d]",td->idx);
        cout << td->feature->getParameterString() <<
                td->feature->getStatisticsString() <<" -> set ";
        printf(" Thresholds[%f]\n",t.ips);
    }

    if (mIPS > t.ips && mInl >= 50) {
        t.ips = mIPS;
        printf("[%03d]",td->idx);
        cout << td->feature->getParameterString() <<
                td->feature->getStatisticsString() <<" -> max " << endl;
    }
    td->feature->ClearData();
}
//==================================================================================================
void LoadImages(const string &path, vector<string> &file_names, vector<double> &time_stamps)
{
    // Setting up full path
    string strPathTimeFile = path + "/times.txt";
    string strPrefixLeft = path + "/image_0/";
    cout << "Time stamp path:" << strPathTimeFile << endl;
    cout << "Images path:" << strPrefixLeft << endl;

    // Reading time stamps and write them to vector
    ifstream fTimes;
    fTimes.open(strPathTimeFile.c_str());
    while(!fTimes.eof())
    {
        string s;
        getline(fTimes,s);
        if(!s.empty())
        {
            stringstream ss;
            ss << s;
            double t;
            ss >> t;
            time_stamps.push_back(t);
        }
    }

    // Generating vector with image full path names
    unsigned long nTimes = time_stamps.size();
    file_names.resize(nTimes);
    for(int i=0; i<nTimes; i++)
    {
        stringstream ss;
        ss << setfill('0') << setw(6) << i;
        file_names[i] = strPrefixLeft + ss.str() + ".png";
    }
}
Mat GrabImage(const string name){
    Mat frame = cv::imread(name);
    if (MAX_NUMBER_PIXEL > 1e5f) {
        double scale = sqrt(MAX_NUMBER_PIXEL / (frame.rows * frame.cols));
        resize(frame, frame, Size(), scale, scale);

        if (scale > 1)
            cout << "Warning: image was enlarged!" << endl;
    }
    return frame;
}
//==================================================================================================
void Iterate(ThreadData<Akaze>* tda){
    // Start values
    auto threshold = new Threshold();

    // Create default values to variate by offset
    auto* par = new Parameter();
    float def_c = par->_akaze._3_threshold;
    int def_d = par->_akaze._4_nOctaves;
    int def_e = par->_akaze._5_nOctaveLayers;

    /// Set default parameters as start max
    tda->feature->setParameters(par);
    Loop(tda);
    Check4Max(tda,*threshold);

    // Iteration vectors
    for(int a : par->_akaze._1_array){
        par->_akaze._1_descriptorSize = a;

        for(int b : par->_akaze._2_array){ // only channel 3!
            par->_akaze._2_descriptorChannels = b;

            for(float c : par->_akaze._3_array){
                par->_akaze._3_threshold = def_c * c;

                for(int d : par->_akaze._4_array){
                    par->_akaze._4_nOctaves = def_d + d;

                    for(int e : par->_akaze._5_array){
                        par->_akaze._5_nOctaveLayers = def_e + e;

                        /// Start Main loop
                        tda->feature->setParameters(par);
                        Loop(tda);
                        Check4Max(tda,*threshold);

                        if(SAVE_RESULTS)
                            tda->log->Statistics("AKAZE",tda->feature->getCurrentStats());

                    }
                }
            }
        }
    } // a
}
//==================================================================================================
void Iterate(ThreadData<Brisk>* tdb){
    // Start values
    auto threshold = new Threshold();

    // Create default values to variate by offset
    auto* par = new Parameter();
    int def_a = par->_brisk._1_threshold;
    int def_b = par->_brisk._2_octaves;
    float def_c = par->_brisk._3_patternScale;

    /// Set default parameters as start max
    tdb->feature->setParameters(par);
    Loop(tdb);
    Check4Max(tdb,*threshold);

    // Iteration vectors
    for(int a : par->_brisk._1_array){
        par->_brisk._1_threshold = def_a + a;

        for(int b : par->_brisk._2_array){
            par->_brisk._2_octaves = def_b + b;

            for(float c : par->_brisk._3_array){
                par->_brisk._3_patternScale = def_c * c;

                /// Start Main loop
                tdb->feature->setParameters(par);
                Loop(tdb);
                Check4Max(tdb,*threshold);

                if(SAVE_RESULTS)
                    tdb->log->Statistics("BRISK",tdb->feature->getCurrentStats());
            }
        }
    } // a

}
//==================================================================================================
void Iterate(ThreadData<Orb>* tdo){
    // Start values
    auto threshold = new Threshold();

    // Create default values to variate by offset
    auto* par = new Parameter();
    int def_a = par->_orb._1_nFeatures;
    float def_b = par->_orb._2_scaleFactor;
    int def_c = par->_orb._3_nLevels;
    int def_d = par->_orb._4_edgeThreshold;
    int def_h = par->_orb._8_fastThreshold;

    /// Set default parameters as start max
    tdo->feature->setParameters(par);
    Loop(tdo);
    Check4Max(tdo,*threshold);

    for(int a : par->_orb._1_array){
        par->_orb._1_nFeatures = a;

        for(float b : par->_orb._2_array){ // not lower 1.0!
            par->_orb._2_scaleFactor = def_b * b;

            for(int c : par->_orb._3_array){
                par->_orb._3_nLevels = def_c + c;

                for(int d : par->_orb._4_array){
                    par->_orb._4_edgeThreshold = def_d + d;

                    for(int e : par->_orb._5_array){
                        par->_orb._5_firstLevel = e;

                        for (int f : par->_orb._6_array){
                            par->_orb._6_WTA_K = f;

                            for(int g : par->_orb._7_array){
                                par->_orb._7_patchSize = g;

                                for(int h : par->_orb._8_array){
                                    par->_orb._8_fastThreshold = def_h + h;

                                    /// Start Main loop
                                    tdo->feature->setParameters(par);
                                    Loop(tdo);
                                    Check4Max(tdo,*threshold);

                                    if(SAVE_RESULTS)
                                        tdo->log->Statistics("ORB",tdo->feature->getCurrentStats());
                                }
                            }
                        }
                    }
                }
            }
        }
    } // a
}
//==================================================================================================
void Iterate(ThreadData<Sift>* tds){
    // Start values
    auto threshold = new Threshold();

    // Create default values to variate by offset
    auto* par = new Parameter();
    int def_b = par->_sift._2_nOctaveLayers;
    double def_c = par->_sift._3_contrastThreshold;
    double def_d = par->_sift._4_edgeThreshold;
    double def_e = par->_sift._5_sigma;

    /// Set default parameters as start max
    tds->feature->setParameters(par);
    Loop(tds);
    Check4Max(tds,*threshold);

    // Iteration vectors
    for(int a : par->_sift._1_array){
        par->_sift._1_nFeatures = a;

        for(int b : par->_sift._2_array){
            par->_sift._2_nOctaveLayers = def_b + b;

            for(double c : par->_sift._3_array){
                par->_sift._3_contrastThreshold = def_c * c;

                for(double d : par->_sift._4_array){
                    par->_sift._4_edgeThreshold = def_d * d;

                    for(double e : par->_sift._5_array){
                        par->_sift._5_sigma = def_e * e;

                        /// Start Main loop
                        tds->feature->setParameters(par);
                        Loop(tds);
                        Check4Max(tds,*threshold);

                        if(SAVE_RESULTS)
                            tds->log->Statistics("SIFT",tds->feature->getCurrentStats());
                    }
                }
            }
        }
    }
}
