//
// Created by behretv on 03.01.19.
//

#include "Akaze.h"

//=================================================================================================
Akaze::Akaze(Parameter* params){
    // Set type
    _type = "AKAZE";

    // Create Object
    _akaze = AKAZE::create();

    // Set parameters
    _akaze->setDescriptorSize(params->_akaze._1_descriptorSize);
    _akaze->setDescriptorChannels(params->_akaze._2_descriptorChannels);
    _akaze->setThreshold(params->_akaze._3_threshold);
    _akaze->setNOctaves(params->_akaze._4_nOctaves);
    _akaze->setNOctaveLayers(params->_akaze._5_nOctaveLayers);

    _parameter = params;
    this->setTracker();
}
//=================================================================================================
Akaze::~Akaze(){
    if(_akaze){
        delete [] _akaze;
        delete [] _tracker;
    }
}
//=================================================================================================
void Akaze::setTracker() {
    Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("BruteForce");
    _tracker = new Tracker(_akaze,matcher);
}
//=================================================================================================
void Akaze::setParameters(Parameter* params) {
    _akaze->setDescriptorSize(params->_akaze._1_descriptorSize);
    _akaze->setDescriptorChannels(params->_akaze._2_descriptorChannels);
    _akaze->setThreshold(params->_akaze._3_threshold);
    _akaze->setNOctaves(params->_akaze._4_nOctaves);
    _akaze->setNOctaveLayers(params->_akaze._5_nOctaveLayers);

    _parameter = params;
    this->setTracker();
}
//=================================================================================================
string Akaze::getParameterString() {
    stringstream info_str;
    info_str << setprecision(4) <<" AKAZE( " <<
    _parameter->_akaze._1_descriptorSize << "| " <<
    _parameter->_akaze._2_descriptorChannels << "|" <<
    _parameter->_akaze._3_threshold << "|" <<
    _parameter->_akaze._4_nOctaves << "|" <<
    _parameter->_akaze._5_nOctaveLayers << ")";

    return info_str.str();
}