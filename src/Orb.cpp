//
// Created by behretv on 03.01.19.
//

#include "Orb.h"

//=================================================================================================
Orb::Orb(Parameter* params)
{
    // Type flag
    _type = "ORB";

    // Create object
    _orb = ORB::create();

    // Set parameters
    _orb->setMaxFeatures(params->_orb._1_nFeatures);
    _orb->setScaleFactor(params->_orb._2_scaleFactor);
    _orb->setNLevels(params->_orb._3_nLevels);
    _orb->setEdgeThreshold(params->_orb._4_edgeThreshold);
    _orb->setWTA_K(params->_orb._6_WTA_K);
    _orb->setPatchSize(params->_orb._7_patchSize);
    _orb->setFastThreshold(params->_orb._8_fastThreshold);

    // Set parameters & tracker
    _parameter = params;
    this->setTracker();
}
//=================================================================================================
Orb::~Orb(){
    if(_orb){
        delete [] _orb;
        delete [] _tracker;
    }
}
//=================================================================================================
void Orb::setTracker() {
    Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("BruteForce");
    _tracker = new Tracker(_orb,matcher);
}
//=================================================================================================
void Orb::setParameters(Parameter* params) {
    _orb->setMaxFeatures(params->_orb._1_nFeatures);
    _orb->setScaleFactor(params->_orb._2_scaleFactor);
    _orb->setNLevels(params->_orb._3_nLevels);
    _orb->setEdgeThreshold(params->_orb._4_edgeThreshold);
    _orb->setWTA_K(params->_orb._6_WTA_K);
    _orb->setPatchSize(params->_orb._7_patchSize);
    _orb->setFastThreshold(params->_orb._8_fastThreshold);

    // Set parameters & tracker
    _parameter = params;
    this->setTracker();
}
//=================================================================================================
string Orb::getParameterString() {
    stringstream info_str;
    info_str << setprecision(4) << " ORB(" <<
    _parameter->_orb._1_nFeatures << "|" <<
    _parameter->_orb._2_scaleFactor << "|" <<
    _parameter->_orb._3_nLevels << "|" <<
    _parameter->_orb._4_edgeThreshold << "|" <<
    _parameter->_orb._5_firstLevel << "|" <<
    _parameter->_orb._6_WTA_K << "|" <<
    _parameter->_orb._7_patchSize<< "|" <<
    _parameter->_orb._8_fastThreshold << ")";

    return info_str.str();
}