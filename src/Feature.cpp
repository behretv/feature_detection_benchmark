//
// Created by behretv on 03.01.19.
//

#include "Feature.h"

//=================================================================================================
// Constructor & destructor
Feature::Feature():
        _previousFrame(1,1,CV_8UC3,cv::Scalar(0,0,0)),
        _currentFrame(1,1,CV_8UC3,cv::Scalar(0,0,0)),
        _boundingBox(4),
        _idx(0),
        _vis(false),
        _log(false),
        _tracker(nullptr)
{
    // If init false -> closed == true
    if(_log) cout << "Init feat_obj..";

    // Template initialization
    Median(vector<int> {0});
    Median(vector<double> {0.0});
}
Feature::~Feature() {
    if(_tracker){
        delete _tracker;
        _tracker = nullptr;
    }
}
//=================================================================================================
// Set
void Feature::setVisualization(bool mode) {
    _vis = mode;
    _tracker->SetVisualization(true);
    namedWindow(_type, WINDOW_AUTOSIZE );
}
void Feature::setCurrentFrame(cv::Mat input_frame){
    // Set images
    if(_log) cout << _currentFrame.size() << " | " << input_frame.size() << endl;

    // Only resize if the first frame has already been set
    if (_currentFrame.rows > 1 && _currentFrame.cols > 1) {
        resize(_currentFrame, _previousFrame, input_frame.size());
    }
    _currentFrame = input_frame;
}
//=================================================================================================
// Methods
//-------------------------------------------------------------------------------------------------
void Feature::ApplyOnImage() {
// Log message
    if (_log) cout << " Initialize feature detection: ";

    // Declarte local variables
    vector<Point2f> bb(4);

    // Compute parameters of the bounding box
    float margin, width, height, h_full;
    Size size = _currentFrame.size();
    margin = 10.0f;
    h_full = (float) size.height - margin * 2;
    width = (float) size.width - margin * 2;
    height = h_full;

    // Set bounding box
    bb[0] = Point2f(margin, margin);
    bb[1] = Point2f(margin, height);
    bb[2] = Point2f(width, height);
    bb[3] = Point2f(width, margin);
    if (_log) std::cout << "Boundingbox: \n " << bb << std::endl;

    // Init matcher handle
    Mat &tempFrame1 = _previousFrame;
    Mat &tempFrame2 = _currentFrame;

    // Start time
    Stats stats;
    int64 t0 = cv::getTickCount();

    _tracker->SetFirstFrame(tempFrame1, bb, stats);
    _tracker->Process(tempFrame2);

    // Stop time
    int64 t1 = cv::getTickCount();
    double sec1 = (t1 - t0) / cv::getTickFrequency();
    _tracker->SetFPS(1 / sec1);
}
//-------------------------------------------------------------------------------------------------
bool Feature::Update() {
    if (_previousFrame.rows <= 1 || _previousFrame.cols <= 1) {
        if(_log) cout << "Warning: Image row or line is smaller or equal 1!" << endl;
        return false;
    }

    // Set features
    if (_tracker) {
        ApplyOnImage();
        _idx++;
        Stats stats = _tracker->GetStats();
        _inliers.push_back(stats.inliers);
        _ratio.push_back(stats.ratio);
        _ips.push_back(stats.inliers*stats.fps);
        _fps.push_back(stats.fps);

    } else {
        wcout << "Tracker-obj = NULL, missing initalization?" << endl;
    }

    // Visualization
    if(_vis){
        ShowImagePair();
    }

    return true;
}
//-------------------------------------------------------------------------------------------------
void Feature::ShowImagePair(){
    // Combine images
    Mat fullImg, resizedImg;
    if(_log) cout << _currentFrame.size() << " | " << _previousFrame.size() << endl;
    if (_currentFrame.rows == _previousFrame.rows) {
        // Resizing to be able to display image on a single screen
        resize(_tracker->GetResult(), resizedImg, Size(), 0.5, 0.5);
        putText(resizedImg, _type, Point(0, 60), FONT_HERSHEY_PLAIN, 5, Scalar::all(0), 4);

        if (resizedImg.rows < 100) {
            cout << "Image size: " << resizedImg.size() << endl;
        }
        //show all in a single window
        imshow(_type, resizedImg);
        waitKey(20);
    }
}
//-------------------------------------------------------------------------------------------------
void Feature::ClearData(){
    _fps.clear();
    _ratio.clear();
    _inliers.clear();
}
//=================================================================================================
// Get
string Feature::getParameterString(){
    stringstream info_str;

    // Safe threshold parameter of each method in stream
    info_str << "Akaze: " << _parameter->_akaze._3_threshold << endl;
    info_str << "Brisk: " << _parameter->_brisk._1_threshold << endl;
    info_str << "Orb: "   << _parameter->_orb._8_fastThreshold<< endl;
    info_str << "Sift: "  << _parameter->_sift._3_contrastThreshold << endl;

    return info_str.str();
}
string Feature::getStatisticsString(){
    stringstream info_str;
    // Safe threshold parameter of each method in stream
    info_str << setprecision(4) << " = {" <<
             getMedianInlier() << ","  <<
             getMedianRatio() << "," <<
             getMedianFPS() << "," <<
             getMedianTFPS() << "}";

    return info_str.str();
}
//==================================================================================================
// Template method
template<typename T> T Feature::Median(vector<T> array){
    sort(array.begin(),array.end());

    // Declare variables & compute size
    int idx, size;
    size = (int)array.size();
    if(size == 0) return 0;

    // Compute idx of the median
    if(size%2 == 0)
        idx = size/2;
    else
        idx = (size-1)/2;

    // Return value
    T val = array[idx];
    return val;
}