//
// Created by behretv on 03.01.19.
//

#include "Brisk.h"

//=================================================================================================
Brisk::Brisk(Parameter* params)
{
    // Type flag
    _type = "BRISK";

    // Create object
    _brisk = BRISK::create(
            params->_brisk._1_threshold,
            params->_brisk._2_octaves,
            params->_brisk._3_patternScale);

    // Set parameters
    _parameter = params;
    this->setTracker();
}
//=================================================================================================
Brisk::~Brisk(){
    if(_brisk){
        delete [] _tracker;
        delete [] _brisk;
    }
}
//=================================================================================================
void Brisk::setTracker() {
    Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("BruteForce");
    _tracker = new Tracker(_brisk,matcher);
}
//=================================================================================================
void Brisk::setParameters(Parameter *params) {
    // Create object
    _brisk = BRISK::create(
            params->_brisk._1_threshold,
            params->_brisk._2_octaves,
            params->_brisk._3_patternScale);

    // Set parameters & tracker
    _parameter = params;
    this->setTracker();
}
//=================================================================================================
string Brisk::getParameterString() {
    stringstream info_str;
    info_str << setprecision(4) <<" BRSIK(" <<
    _parameter->_brisk._1_threshold << "| " <<
    _parameter->_brisk._2_octaves << "|" <<
    _parameter->_brisk._3_patternScale << ")";

    return info_str.str();
}